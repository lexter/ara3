/*
 * decode.cpp
 *
 *  Created on: 03 ����. 2014 �.
 *      Author: User
 */

#include "decode.h"

char decode::decode_en(int in_char) {
		switch (in_char) {
			case 65:
				return 'a';
			case 66:
				return 'b';
			case 67:
				return 'c';
			case 68:
				return 'd';
			case 69:
				return 'e';
			case 70:
				return 'f';
			case 71:
				return 'g';
			case 72:
				return 'h';
			case 73:
				return 'i';
			case 74:
				return 'j';
			case 75:
				return 'k';
			case 76:
				return 'l';
			case 77:
				return 'm';
			case 78:
				return 'n';
			case 79:
				return 'o';
			case 80:
				return 'p';
			case 81:
				return 'q';
			case 82:
				return 'r';
			case 83:
				return 's';
			case 84:
				return 't';
			case 85:
				return 'u';
			case 86:
				return 'v';
			case 87:
				return 'w';
			case 88:
				return 'x';
			case 89:
				return 'y';
			case 90:
				return 'z';
		}
		return char(in_char);

}

char decode::decode_ru(int in_char) {
		switch (in_char) {
			case 65:
				return '�';
			case 66:
				return '�';
			case 67:
				return '�';
			case 68:
				return '�';
			case 69:
				return '�';
			case 70:
				return '�';
			case 71:
				return '�';
			case 72:
				return '�';
			case 73:
				return '�';
			case 74:
				return '�';
			case 75:
				return '�';
			case 76:
				return '�';
			case 77:
				return '�';
			case 78:
				return '�';
			case 79:
				return '�';
			case 80:
				return '�';
			case 81:
				return '�';
			case 82:
				return '�';
			case 83:
				return '�';
			case 84:
				return '�';
			case 85:
				return '�';
			case 86:
				return '�';
			case 87:
				return '�';
			case 88:
				return '�';
			case 89:
				return '�';
			case 90:
				return '�';
		}
		return char(in_char);
}

char decode::decode_numb_up(int in_char,bool up) {
	if (up) {
		switch (in_char) {
				case 48:
					return ')';
				case 49:
					return '!';
				case 50:
					return '@';
				case 51:
					return '#';
				case 52:
					return '$';
				case 53:
					return '%';
				case 54:
					return '^';
				case 55:
					return '&';
				case 56:
					return '*';
				case 57:
					return '(';
				case 189:
					return '_';
				case 187:
					return '+';
			}
			return char(in_char);
	} else {
		switch (in_char) {
			case 189:
				return '-';
			case 187:
				return '=';
		}
		return char(in_char);
	}
}
